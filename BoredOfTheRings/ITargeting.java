package BoredOfTheRings;

public interface ITargeting {

    void hit(Enemy e);

    void collide(Enemy e);

}