package BoredOfTheRings;

public class Human extends Enemy {

    public Human()
    {
        speed = defaultSpeed = 0.75f;
        health = 140;
        position = destination = new MyVector(0, 0);
        curPath = 0;
        img = Resource.getInstance().getImage(7);
        //randomgeneralunk egy kovetendo utat az ellensegnek
        int pathToFollow = ((int)(Math.random() * 1000.0)) % level.pathes.size();
        init(pathToFollow, level.pathes.get(pathToFollow).get(0));
    }

    public Human(Human e)
    {
        health = e.health;
        curPath = e.curPath;
        speed = e.speed;
        defaultSpeed = e.defaultSpeed;
        position = e.position;
        destination = e.destination;
        img = e.img;
        imgWidth = e.imgWidth;
        imgHeight = e.imgHeight;

        System.out.println(name + "     SLASHED ENEMY CREATED (" + position.x + ", " + position.y + "),  " + curPath + ", ("  + destination.x + ", " + destination.y + ")");
    }


    public void hitByStone(Projectile p)
    {
        System.out.println("-->Human.checkHealth()");
        health -= p.dmg;
        level.player.mana += 5;
        if(checkHealth())
        {
            level.deleteEntity(this);
            level.deleteEnemy(this);
        }
    }

    public void hitByDarkSpell(Projectile p)
    {
        System.out.println("-->Human.checkHealth()");
        health -= p.dmg;
        level.player.mana += 5;
        if(checkHealth())
        {
            level.deleteEntity(this);
            level.deleteEnemy(this);
        }
    }

    public void hitByArrow(Projectile p)
    {
        System.out.println("-->Human.checkHealth()");
        health -= p.dmg * 1.25;
        level.player.mana += 5;
        System.out.println(health);
        if(checkHealth())
        {
            level.deleteEntity(this);
            level.deleteEnemy(this);
        }
    }

    public void hitByEnergyBall(Projectile p)
    {
        System.out.println("-->Human.checkHealth()");
        health -= p.dmg;
        level.player.mana += 5;
        if(checkHealth())
        {
            level.deleteEntity(this);
            level.deleteEnemy(this);
        }
    }

    public void hitByIceCube(Projectile p)
    {
        System.out.println("-->Human.checkHealth()");
        health -= p.dmg * 0.75;
        level.player.mana += 5;
        if(checkHealth())
        {
            level.deleteEntity(this);
            level.deleteEnemy(this);
        }
    }

    public void hitBySlasher(Projectile p)
    {
        System.out.println("-->Human sliced in half! Pos: (" + position.x + ", " + position.y + "), Dest: (" + destination.x + ", " + destination.y + ")" );
        health /=2;
        Human cl = new Human(this);
        cl.setName(this.getName() + "_SLASHED");
        level.player.mana += 5;
        level.pushEnemy(cl);
        level.setRemainingEnemies(level.getRemainingEnemies() + 1);
        checkHealth();
    }

    public String toString()
    {
        String s = String.format("<e><%s> : < \n" +
                "defaultSpeed : %f; \n" +
                "speed: %f; \n" +
                "health: %d; \n" +
                "curPath: %d; \n" +
                "position: %s; \n" +
                "destination: %s; \n" +
                "name: %s \n", getClass().getSimpleName(), defaultSpeed, speed, health, curPath, position.toString(), destination.toString(), name);
        return s;
    }
}