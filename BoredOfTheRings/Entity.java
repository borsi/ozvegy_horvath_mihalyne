package BoredOfTheRings;

import java.awt.*;

public abstract class Entity{
    protected static Level level = new Level();
    protected String name;
    protected MyVector position, destination;
    public Image img, fogImg = null;
    protected boolean inFog = false;
    public int imgWidth, imgHeight;

    public void setName(String name) {
        this.name = name;
    }

    public MyVector getPosition()
    {
        return position;
    }

    public void setPosition(MyVector position) {
        this.position = position;
    }

	public void step(float dt) { }

    public String getName() {
        return name;
    }
}