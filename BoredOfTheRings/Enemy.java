package BoredOfTheRings;

import BoredOfTheRings.Projectile;

public abstract class Enemy extends Entity
{
	protected int health, curPath;
    protected float speed, defaultSpeed;
    protected MyVector position, destination;

    public void init(int pathToFollow, MyVector initialPosition)
    {
        curPath = pathToFollow;
        destination = level.getNextDestination(curPath, initialPosition);
        position.x = initialPosition.x;
        position.y = initialPosition.y;
        //System.out.println("-->level.getNextDestination(Position: (" + position.x + ", " + position.y + "),  " + curPath + ", ("  + destination.x + ", " + destination.y + "): MyVector)");
        imgHeight = imgWidth = 45;
    }

 	public void hitByStone(Projectile p) { }

	public void hitByDarkSpell(Projectile p) { }

	public void hitByArrow(Projectile p) { }

	public void hitByEnergyBall(Projectile p) {	}

	public void hitByIceCube(Projectile p) { }

	public void hitByObstacle(float slowRate) {
		speed -= slowRate;
	}

    public void hitBySlasher(Projectile p) { }

    public boolean checkHealth()
    {
        if(health <= 0) {
            System.out.println("\t<-- " + this.getName() + " is shot dead");
            return true;
        }
        else {
            System.out.println("\t<-- " + this.getName() + " got shot, still going");
            System.out.println("\t"+ this.getName() + "'s life left: " + health);
            return false;
        }
    }

   /* public String toString() {
    	String s = String.format("<e><%s> : < \n" +
                "defaultSpeed : %f; \n" +
                "speed: %f; \n" +
                "health: %d; \n" +
                "curPath: %d; \n" +
                "position: %s; \n" +
                "destination: %s; \n" +
                "name: %s \n", getClass().getSimpleName(), defaultSpeed, speed, health, curPath, position.toString(), destination.toString(), name);
        return s;
    }*/

    @Override
    public MyVector getPosition()
    {
        return position;
    }

   @Override
   public void step(float dt)
   {
        MyVector offset = new MyVector(destination);
        offset.substract(position);
        offset.normalize();
        offset.multiply(dt/250f * speed);
        position.add(offset);

        MyVector newOffset = new MyVector(destination);
        newOffset.substract(position);
        newOffset.normalize();
        System.out.println("STEPPING " + name);

       //Ha elojelet valtottunk, kerjuk a kovetkezo celpontot.
        if(((offset.x > 0 && newOffset.x <= 0) || (offset.x <= 0 && newOffset.x > 0) || (offset.y > 0 && newOffset.y <= 0) || (offset.y <= 0 && newOffset.y > 0)))
        {
            destination = level.getNextDestination(curPath, destination);
            System.out.println(name + "      -->level.getNextDestination(Position: (" + position.x + ", " + position.y + "),  " + curPath + ", ("  + destination.x + ", " + destination.y + "): MyVector) SPEED " + speed);
        }
        //System.out.println(this.getName() + " is at: " + this.position);
        speed = defaultSpeed;
   }
}