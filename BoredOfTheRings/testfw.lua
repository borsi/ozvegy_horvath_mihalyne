function doBindings()
    -- classes
    ArrayList = luajava.bindClass('java.util.ArrayList')
    MyVector = luajava.bindClass('BoredOfTheRings.MyVector')
    Level = luajava.bindClass('BoredOfTheRings.Level')

    ArrowTower = "ArrowTower"
    DarkTower = "DarkTower"
    Dwarf = "Dwarf"
    Elf = "Elf"
    EnergyTower = "EnergyTower"
    Hobbit = "Hobbit"
    Human = "Human"
    IceTower = "IceTower"
    Obstacle = "Obstacle"
    Tower = "Tower"
    EnergyBall = "EnergyBall"
    Arrow = "Arrow"
    
    TIMESTEP = 100.0
    -- objects
    cli = luajava.newInstance('BoredOfTheRings.CommandInterface')
end

function createMap(roads)
    local roads_list = ArrayList.new()

    for i, road in pairs(roads) do
        local road_obj = ArrayList.new()
        
        for j, s in pairs(road) do
            v = MyVector.new(s[1], s[2])
            road_obj:add(v)
        end

        roads_list:add(road_obj)
    end

    cli:createMap(roads_list)
end

function findEntity(name)
    local entities = cli:getLevel():getEntities()

    for i = 0, entities:size()-1 do
        local entity = entities:get(i)
        if entity:getName() == name then 
            return entity 
        end
    end

    return nil
end

function list(name)
    local entity = findEntity(name)
    if entity then
        print(entity:toString())
    else
        if name == "ALL" then
            local entities = cli:getLevel():getEntities()
            for i = 0, entities:size()-1 do
                local e = entities:get(i)
                print(e:toString())
            end
        elseif name == "level" then
            print(cli:getLevel():toString())
        else
            print("no entity found with name " .. name)
        end
    end
end

function step(name)
    local entity = findEntity(name)
    if entity then
        entity:step(TIMESTEP)
    else
        if name == "ALL" then
            local entities = cli:getLevel():getEntities()
            for i = 0, entities:size()-1 do
                local e = entities:get(i)
                e:step(TIMESTEP)
            end
        elseif name == "level" then
            cli:getLevel():step(TIMESTEP)
        end
    end
end

function create(class, name)
    class_name = 'BoredOfTheRings.' .. class

    entity = luajava.newInstance(class_name)
    if not entity then
        print('Could not create entity with class ' .. class)
        return nil
    end

    entity:setName(name)
    cli:getLevel():pushEntity(entity)
    return entity
end

function createEnemy(class, name)
    class_name = 'BoredOfTheRings.' .. class

    entity = luajava.newInstance(class_name)
    if not entity then
        print('Could not create entity with class ' .. class)
        return nil
    end

    entity:setName(name)
    cli:getLevel():pushEntity(entity)
    cli:getLevel():pushEnemy(entity)
    return entity
end

function add(coord, e)
    -- todo
    local x = coord[1]
    local y = coord[2]
    local tile = cli:getLevel():getTile(x, y)

    e:setPosition(MyVector.new(x, y))
    tile:buildOn(e)
end

function setPosition(coord, e)
    local x = coord[1]
    local y = coord[2]
    e:setPosition(MyVector.new(x, y)) 
end

function initEnemy(coord, e, pathid)
    local x = coord[1]
    local y = coord[2]
    e:init(pathid, MyVector.new(x, y))
end

function setRemainingEnemies(n)
    cli:getLevel():setRemainingEnemies(n)
end

function test_createMap()
    local roads = { 
        { { 0, 1 }, { 0, 3 }, { 5, 2 } }, 
        { { 3, 1 }, { 6, 3 }, { 8, 0 } }, 
    }

    createMap(roads)
end

function test_toString()
    cli:testTowerToString()
end

function foo()
    test_createMap()

    t = create(Tower, "tower1")
    list("tower1")
    add({0, 5}, t)
    list("tower1")
    create(Hobbit, "hi")

    cli:getLevel():saveToFile("level.txt")
end

function bar()
    cli:loadLevel("level.txt")
    list("ALL")
end

doBindings()
--test_createMap()
--bar()
