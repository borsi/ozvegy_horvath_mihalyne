package BoredOfTheRings;

public class Road extends Tile
{
    public Road()
    {
        tower = null;
        obstacle = null;
        System.out.println("\t<-- Road object created");
        this.img = Resource.getInstance().getImage(1);
    }

	public void upgrade(IUpgrade u) { }
	
	public void buildOn(IBuilding b)
    {
        if(obstacle == null)
        {
            b.buildOnRoad(this);
        }
	}
}