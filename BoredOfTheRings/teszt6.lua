--az utak helyének definiálása
local roads = {
        { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 3, 2 }, { 4, 2 }, { 5, 2 }, { 5, 3 }, { 5, 4 }, { 5, 5 }, { 5, 6 }, { 5, 7 } },
        { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 2, 3 }, { 2, 4 }, { 2, 5 }, { 2, 6 }, { 3, 6 }, { 4, 6 }, { 5, 6 }, { 5, 7 } },
}

--létrehozom a pályát úttal
createMap(roads)

--torony, akadály, elf létrehozása
t = create(DarkTower, "darkTower1")
o = create(Obstacle, "obstacle1")
-- createEnemy, mert belepusholjuk az enemy-k tarolojaba is
e = createEnemy(Elf, "elf1")

--kicsit messzebb raktam a tornyot, hogy belépjen a hatókörébe
add({1, 2}, t)
add({2, 3}, o)
--setPosition({2, 0}, e)

initEnemy({2, 0}, e, 1)
--annyit kell lépni hogy beérjen a torony hatókorebe és az elkezdjen rá tüzelni
for i=1, 15, 1 do
    print("\n" .. i .. ". step")
    step("ALL")
end

--listázás
list("darkTower1")
list("obstacle1")
list("elf1")
list("level")
