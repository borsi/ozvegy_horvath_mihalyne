package BoredOfTheRings;

import javafx.geometry.Pos;

import java.awt.*;
import java.util.List;
import java.util.ArrayList;

public class Tower extends Entity implements IBuilding, IUpgrade {

	protected float dmg, fireRate, range, temp_range;
    protected float countDownTimer;
    protected List<Enemy> enemiesInRange = null;
    protected float fog, fogTime;

    public Tower()
    {
        enemiesInRange = new ArrayList<Enemy>();
       //System.out.println("--> <<create>> t:Tower");
        dmg = 50;
        fireRate = 220;
        range = 3f;
        position = new MyVector(0, 0);
        countDownTimer = (float)fireRate;
        fogTime = 0.0f;
        fog = 1.0f;
        temp_range = range;
        img = Resource.getInstance().getImage(2);
        fogImg = Resource.getInstance().getImage(18);
        imgWidth = 64;
        imgHeight = 64;
    }

    public Tower(MyVector p)
    {
        enemiesInRange = new ArrayList<Enemy>();
       // System.out.println("--> <<create>> t:Tower");
        dmg = 50;
        fireRate = 220;
        range = 3f;
        position = p;
        countDownTimer = (float)fireRate;
        fogTime = 0.0f;
        fog = 1.0f;
        temp_range = range;
        img = Resource.getInstance().getImage(2);
        fogImg = Resource.getInstance().getImage(18);
        imgWidth = 64;
        imgHeight = 64;
    }

    public Projectile shoot(Enemy enemy)
    {
        //System.out.print("t:Tower");
        //random generalunk slashert vagy nem.
        Projectile p;
        int randomNum = (int)(Math.random()*100);
        MyVector m = new MyVector(this.position);
        System.out.println("RANDOM " + randomNum);
        if(randomNum > 95)
        {
            System.out.println("SLAAAASHEEEEEEEEEEEER");
            p = new Slasher(enemy, this.dmg, m);
        }
        else
        {
            p = new Projectile(enemy, this.dmg, m);
        }
        return p;
	}

    public void upgradeRange()
    {
        System.out.println("\t<-- Rangedsdasdasdd!" + range);
        temp_range *= 1.1f;
        System.out.println("\t<-- Range upgraded!" + range);
    }

    public void upgradeEffect()
    {
        dmg *= 1.1f;
        System.out.println("\t<-- Damage upgraded! "  + dmg);
    }

    public void upgradeRate()
    {
        fireRate *= .9f;
        System.out.println("\t<-- Rate of firing upgraded!");
    }

    public void specializeIceTower()
    {
        IceTower t = new IceTower();
        t.position = this.position;
        t.range = this.range;
        t.dmg = this.dmg;
        t.fireRate = this.fireRate;
        t.enemiesInRange = this.enemiesInRange;
        level.pushEntity(t);
        level.deleteEntity(this);
    }

    public void specializeEnergyTower()
    {
        EnergyTower t = new EnergyTower();
        t.position = this.position;
        t.range = this.range;
        t.dmg = this.dmg;
        t.fireRate = this.fireRate;
        t.enemiesInRange = this.enemiesInRange;
        level.pushEntity(t);
        level.deleteEntity(this);
    }

    public void specializeArrowTower()
    {
        ArrowTower t = new ArrowTower();
        t.position = this.position;
        t.range = this.range;
        t.dmg = this.dmg;
        t.fireRate = this.fireRate;
        t.enemiesInRange = this.enemiesInRange;
        level.pushEntity(t);
        level.deleteEntity(this);
    }

    public void specializeDarkTower()
    {
        DarkTower t = new DarkTower();
        t.position = this.position;
        t.range = this.range;
        t.dmg = this.dmg;
        t.fireRate = this.fireRate;
        t.enemiesInRange = this.enemiesInRange;
        level.pushEntity(t);
        level.deleteEntity(this);
    }

    public String getEnemiesInRangeStr()
    {
        StringBuilder sb = new StringBuilder();
        if(enemiesInRange.size() != 0)
        {
            sb.append("[");
            for (Entity e : enemiesInRange)
            {
                sb.append(String.format("\"%s\"", e.getName()));
                sb.append(", ");
            }
            String s = sb.toString().substring(0, sb.length() - 2) + "]";
            return s;
        }
        else  return "[]";
    }

    @Override
    public void step(float dt)
    {
        // a torony lepese metodus
        // megtolti a torony enemy tarolojat a
        // level ellensegekkel
        level.fillEnemiesInRange(this);

        // ha lejar a szamlalo, lovunk, ha van valaki a range-ben
        // ha nincs senki a range-ben, a timer marad 0, vagy az alatt
        // a torony "csore van toltve"

        // a firerate szamlalot csokkentjuk
        if(countDownTimer <= 0)
        {
            if(!enemiesInRange.isEmpty())
            {
                level.pushEntity(this.shoot(enemiesInRange.get(0)));
                countDownTimer = (float) fireRate;
            }
        }
        else
        {
            countDownTimer -= dt;
        }

        //kod szall le, a kod csokkenti
        //a tornyok hatotavjat (ideiglenesen)
        int randomNum = (int)(Math.random() * 100000);
        if(randomNum >= 99995)
        {
            fogTime += 400.0f;
            System.out.println(this.getName() + " is under heavy fog.");
        }
        if(fogTime > 0f)
        {
            inFog = true;
            fog = 0.75f;
            fogTime -= dt;
        }
        else if(inFog)
        {
            inFog = false;
            fogTime = 0f;
            fog = 1.0f;
        }
        range = fog * temp_range;
    }

    public boolean isInFog()
    {
        return inFog;
    }

	@Override
	public void buildOnRoad(Tile tile)
    {
        System.out.println("<-- false - cannot build on road");
	}

    @Override
    public void setPosition(MyVector v)
    {
        position = v;
    }

	@Override
	public void buildOnPlain(Tile tile)
    {
        System.out.println("<-- true - built on plain");
        level.player.mana -= 40;
        tile.tower = this;
        level.pushEntity(this);
	}

    @Override
    public String toString(){
        String s = String.format("<t><%s> : < \n" +
                "enemiesInRange : %s; \n" +
                "dmg: %f; \n" +
                "fireRate: %f; \n" +
                "range: %f; \n" +
                "position: %s; \n" +
                "fogTime: %f; \n" +
                "fog: %f; \n" +
                "name: %s \n", getClass().getSimpleName(), getEnemiesInRangeStr(), dmg, fireRate, range, position.toString(), fogTime, fog,  name);

        return s;
    }

}