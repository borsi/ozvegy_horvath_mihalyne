package BoredOfTheRings;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by borsi on 3/30/14.
 */
public class CommandInterface {

    public Level getLevel() {
        return level;
    }

    //private Level level = new Level();
    Level level = Entity.level;
    Player player = Level.player;

    public void createMap(ArrayList<List<MyVector>> roads) {
        final MyVector levelSize = new MyVector(8,8);
        level.createMap(levelSize, roads);
    }

    public void loadLevel(String fileName) throws FileNotFoundException {
        level = Level.loadFromFile(fileName);
    }

    public void saveLevel(String fileName) throws FileNotFoundException, UnsupportedEncodingException {
        level.saveToFile(fileName);
    }

    public void testTowerToString() {
        MyVector v = new MyVector(5.0f, 5.0f);
        IceTower t = new IceTower(v);
        System.out.println(t);
    }
}


