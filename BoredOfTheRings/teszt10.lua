--az utak helyének definiálása
local roads = {
        { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 3, 2 }, { 4, 2 }, { 5, 2 }, { 5, 3 }, { 5, 4 }, { 5, 5 }, { 5, 6 }, { 5, 7 } },
        { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 2, 3 }, { 2, 4 }, { 2, 5 }, { 2, 6 }, { 3, 6 }, { 4, 6 }, { 5, 6 }, { 5, 7 } },
}

--létrehozom a pályát úttal
createMap(roads)

--torony, akadály, ember létrehozása
t = create(Tower, "tower1")
o = create(Obstacle, "obstacle1")
e = createEnemy(Human, "human1")

--tile-hoz rendelem őket
add({1, 0}, t)
add({2, 3}, o)

initEnemy({2, 0}, e, 1)
--lövési távolság fejlesztése
t:upgradeRange()
--listázás
list("tower1")
--lövési gyakoriság fejlesztése
t:upgradeRate()
--listázás
list("tower1")
--sebzés fejlesztése
t:upgradeEffect()
--listázás
list("tower1")

o:upgradeEffect()
--listázás
list("obstacle1")


