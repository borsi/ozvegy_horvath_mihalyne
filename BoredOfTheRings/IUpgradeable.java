package BoredOfTheRings;

public interface IUpgradeable {

	void upgrade(IUpgrade u);

}