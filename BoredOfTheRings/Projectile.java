package BoredOfTheRings;

public class Projectile extends Entity implements ITargeting {

    public float dmg;
	protected Enemy target;
    protected float speed;
    protected MyVector position;

    public Projectile() { }

	public Projectile(Enemy t, float damage, MyVector pos)
    {
        dmg = damage;
        target = t;
        speed = 1000;
        position = pos;
        setName("Kooooooooooo");
        System.out.println("/t--> <<create>> p:Projectile("+ t.toString() + "," + dmg + ", position)");
        this.img = Resource.getInstance().getImage(16);
        imgHeight = imgWidth = 28;
	}

    @Override
    public void collide(Enemy e)
    {

    }

    @Override
    public MyVector getPosition()
    {
        return position;
    }

    @Override
    public void hit(Enemy e)
    {
        System.out.println("-->enemy.hitByStone(this: Stone)");
        e.hitByStone(this);
        level.deleteProjectile(this);
    }

    public void step(float dt)
    {
        MyVector offset = new MyVector(target.position);
        offset.substract(position);
        offset.normalize();
        offset.multiply(dt * 100 / speed);
        position.add(offset);

        MyVector newOffset = new MyVector(target.position);
        newOffset.substract(position);
        newOffset.normalize();

        //Ha elojelet valtottunk, kerjuk a kovetkezo celpontot.
        if((offset.x > 0f && newOffset.x <= 0f) || (offset.x <= 0f && newOffset.x > 0f) || (offset.y > 0f && newOffset.y <= 0f) || (offset.y <= 0f && newOffset.y > 0f) || position.equals(target.position))
        {
            this.hit(target);
        }
	}

    @Override
    public String toString()
    {
        String s = String.format("<p><%s> : < \n" +
                "dmg: %d; \n" +
                "speed: %f; \n" +
                "position: %s; \n" +
                "target: %s; \n" +
                "name: %s \n", getClass().getSimpleName(), dmg, speed, position.toString(), target.toString(), name);

        return s;
    }
}