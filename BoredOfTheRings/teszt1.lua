--az utak helyének definiálása
local roads = {
        { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 3, 2 }, { 4, 2 }, { 5, 2 }, { 5, 3 }, { 5, 4 }, { 5, 5 }, { 5, 6 }, { 5, 7 } }, 
        { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 2, 3 }, { 2, 4 }, { 2, 5 }, { 2, 6 }, { 3, 6 }, { 4, 6 }, { 5, 6 }, { 5, 7 } }, 
}

--létrehozom a pályát úttal
createMap(roads)

--torony, akadály, ember létrehozása
t = create(Tower, "tower1")
o = create(Obstacle, "obstacle1")
e = create(Human, "human1")

--tile-hoz rendelem őket
add({1, 0}, t)
add({2, 3}, o)
setPosition({2, 0}, e)

--listázás
list("tower1")
list("obstacle1")
list("human1")
list("level")
