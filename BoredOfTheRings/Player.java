package BoredOfTheRings;

public class Player {

	public int mana;
	private boolean dead;
    private String name;

    public Player() {
        this.mana = 100;
        this.dead = false;
        this.name = "karoly";
    }
	public void setDead() {
		this.dead = true;
	}
    public boolean getPlayerState() {
        return this.dead;
    }
	
    @Override
    public String toString() {
        String s = String.format("<j><%s> : < \n" +
                "mana: %d; \n" +
                "dead: %b; \n" +
                "name: %s \n", getClass().getSimpleName(), mana, getPlayerState(), name);

        return s;
    }
}