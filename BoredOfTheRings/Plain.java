package BoredOfTheRings;

public class Plain extends Tile {

    public Plain()
    {
        tower = null;
        obstacle = null;
        System.out.println("\t<-- Plain object created");
        this.img = Resource.getInstance().getImage(0);
    }

	public void upgrade(IUpgrade u) { }
	
	public void buildOn(IBuilding b)
    {
		if(tower == null)
        {
            b.buildOnPlain(this);
        }
	}
}