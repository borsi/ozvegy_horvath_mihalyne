package BoredOfTheRings;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by borsos1zolta04 on 2014.04.27..
 */
public class GameView extends JPanel
{

    public GameView(Level theLevel)
    {
        super.setLayout(new BorderLayout());
        level = theLevel;
        setPreferredSize(new Dimension(600, 600));
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        //Graphics2D g2d = (Graphics2D)g;
        drawTiles(g);
        drawEntities(g);
    }

    private void drawTiles(Graphics g)
    {
        Tile[][] tiles = level.getTiles();
        for (int i = 0; i < tiles.length; i++)
        {
            for (int j = 0; j < tiles[i].length; j++)
            {
                Tile t = tiles[i][j];

                double tileWidth = (double)getWidth() / (double)tiles[i].length; // pixelszélesség/hány darab tile van
                double tileHeight = (double)getHeight() / (double)tiles.length; //  pixelszélesség/hány sornyi tile van

                int posX = (int)Math.round(i * tileWidth); // x pozíció = hányadik darab balról jobbra * tile szélesség
                int posY = (int)Math.round(j * tileHeight); // y pozíció = hányadik darab fentről lefelé * tile magasság

                Image tileImg = t.img;
                g.drawImage(tileImg, posX, posY, (int) Math.round(tileWidth), (int) Math.round(tileHeight), null);
                this.revalidate();
            }
        }
    }

    private void drawEntities(Graphics g)
    {
        List<Entity> entities = level.getEntities();

        // ez most azért kell, hogy kiszámoljuk az átváltást pixel-koordinátákra
        Tile[][] tiles = level.getTiles();


        int tileWidth = getWidth() / tiles[0].length; // pixelszélesség/hány darab tile van
        int tileHeight = getHeight() / tiles.length; //  pixelszélesség/hány sornyi tile van

        for (Entity e : entities)
        {
            if(e != null)
            {
                //System.out.println("DRAWING " + e.getName());
                MyVector v = e.getPosition();
                if(v != null)
                {
                    //System.out.println("DRAWING " + e.getName() + " DONE");
                    int posX = Math.round(v.x * (float)tileWidth) + ((tileWidth - e.imgWidth) / 2);
                    int posY = Math.round(v.y * (float)tileHeight) + ((tileWidth - e.imgHeight) / 2);
                    //System.out.println("DRAWING " + e.imgWidth + " DONE");
                    //System.out.println(v.x + ";;; " + v.y);
                    //System.out.println("POSX: "+ posX + " posy: " + posY);
                    //System.out.println("POSX: "+ v.x + " posy: " + v.y);
                    //System.out.println(posX + "; " + posY);
                    Image entityImg = e.img; // ezt valahonnan előkaparod
                    g.drawImage(entityImg, posX, posY, e.imgWidth, e.imgHeight, null);

                    if(e.fogImg != null)
                    {
                        if(e.inFog)
                        {
                            g.drawImage(e.fogImg, posX, posY, e.imgWidth, e.imgHeight, null);
                        }
                    }

                    //g.drawImage(entityImg, 3, 3, 19, 19, null);
                    //System.out.println("painting at: " + e.getPosition());
                    this.revalidate();
                }
            }
        }

    }

    private Level level;
}

