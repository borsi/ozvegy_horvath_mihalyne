package BoredOfTheRings;
import java.awt.*;
import java.io.*;
import java.util.List;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.JsePlatform;

// a kirajzolashoz
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;



/*
    Ez az osztály most arra van, hogy tesztelje a játék futását,
    egy többszintű menürendszerben lehet oda-vissza navigálni és
    tesztelni az egyes játékfunkciókat.
 */

class Game extends JFrame{
    public static long lastStepCallTime = 0;
    public static boolean didStep = false;
    public static JFrame frame = new JFrame("BoredOfTheRings");
    public static JPanel upgrade = new JPanel();

    final public class UjJatek extends Thread{
        public void run(){

            //            { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 3, 2 }, { 4, 2 }, { 5, 2 }, { 5, 3 }, { 5, 4 }, { 5, 5 }, { 5, 6 }, { 5, 7 } },
//            { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 2, 3 }, { 2, 4 }, { 2, 5 }, { 2, 6 }, { 3, 6 }, { 4, 6 }, { 5, 6 }, { 5, 7 } },
//        }

            Game.this.setVisible(false);

            JPanel panel = new JPanel();
            JLabel mana_label = new JLabel("Mana: " + Integer.toString(Level.player.mana));

            frame.setLayout(new BorderLayout());
            //upgrade.setLayout(new BoxLayout(upgrade, BoxLayout.Y_AXIS));
            upgrade.setLayout(new GridLayout(11,1));
            upgrade.setBorder(BorderFactory.createLineBorder(Color.BLACK));
            panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

            panel.add(mana_label);

            frame.setSize(600, 600);


            try {
                Resource.getInstance().init();
            } catch (IOException e) {
                e.printStackTrace();
            }

            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            Level level = new Level();
            List<List<MyVector>> roads = new ArrayList<List<MyVector>>();
            List<MyVector> road1 = new ArrayList<MyVector>();
            road1.add(new MyVector(2, 0));
            road1.add(new MyVector(2, 1));
            road1.add(new MyVector(2, 2));
            road1.add(new MyVector(3, 2));
            road1.add(new MyVector(4, 2));
            road1.add(new MyVector(5, 2));
            road1.add(new MyVector(5, 3));
            road1.add(new MyVector(5, 4));
            road1.add(new MyVector(5, 5));
            road1.add(new MyVector(5, 6));
            road1.add(new MyVector(5, 7));

            List<MyVector> road2 = new ArrayList<MyVector>();
            road2.add(new MyVector(2, 0));
            road2.add(new MyVector(2, 1));
            road2.add(new MyVector(2, 2));
            road2.add(new MyVector(2, 3));
            road2.add(new MyVector(2, 4));
            road2.add(new MyVector(2, 5));
            road2.add(new MyVector(3, 5));
            road2.add(new MyVector(4, 5));
            road2.add(new MyVector(5, 5));
            road2.add(new MyVector(5, 6));
            road2.add(new MyVector(5, 7));

            roads.add(road1);
            roads.add(road2);

            level.createMap(new MyVector(8, 8), roads);

            GameView view = new GameView(level);
            view.addMouseListener(new GameController(level));


            frame.add(view, BorderLayout.CENTER);
            frame.add(panel, BorderLayout.PAGE_START);
            frame.add(upgrade, BorderLayout.LINE_END);
            frame.pack();
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);

           /* Human karoly = new Human();
            //karoly.init(0, new MyVector(2.0f, 0f));
            //karoly.setPosition(new MyVector(5f, 6f));
            karoly.setName("karoly");
            level.pushEnemy(karoly);
            level.pushEntity(karoly);
            level.setRemainingEnemies(1);*/
            /*Tower t = new Tower();
            t.setPosition(new MyVector(0, 0));
            Tile tile = level.getTile(0, 0);
            tile.buildOn(t);
            t.setName("zsolti");
            level.pushEntity(t);*/

            while (true) {
                if (!didStep) {
                    didStep = true;
                    lastStepCallTime = System.currentTimeMillis();
                } else {
                    try {
                        this.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    level.step((float) (System.currentTimeMillis() - lastStepCallTime) * 0.1f);
                    lastStepCallTime = System.currentTimeMillis();
                    view.repaint();
                    mana_label.setText("Mana: " + Integer.toString(Level.player.mana));
                }

            }
        }

    }

    final public class NewGameListener implements ActionListener {
        public void actionPerformed(ActionEvent arg0) {
            UjJatek ng = new UjJatek();
            ng.start();
        }
    }

    final public class ExitListener implements ActionListener{
        public void actionPerformed(ActionEvent arg0) {
            System.exit(0);
        }
    }

    public Game(){
        //JFrame frame = new JFrame("BoredOfTheRings");
        this.setName("BoredOfTheRings");
        JPanel panel = new JPanel();
        JButton newgame = new JButton("New Game");
        JButton exit = new JButton("Exit");

        panel.setLayout(null);

        Insets insets = panel.getInsets();
        panel.add(newgame);
        panel.add(exit);

        Dimension size = newgame.getPreferredSize();
        newgame.setBounds(240 + insets.left, 200 + insets.top,
                size.width + 40, size.height + 10);
        size = exit.getPreferredSize();
        exit.setBounds(240 + insets.left, 300 + insets.top,
                size.width+80, size.height+10);

        this.setSize(600, 600);
        this.add(panel);

        newgame.addActionListener(new NewGameListener());
        exit.addActionListener(new ExitListener());

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public static void printMainMenu() {
        System.out.println("Menü:");
        System.out.println("1. Torony léptetése");
        System.out.println("2. Pályainicializálás");
        System.out.println("3. Építés");
        System.out.println("4. Ellenség léptetése");
        System.out.println("5. Akadály léptetése");
        System.out.println("6. Lövedék léptetése");
        System.out.println("7. Kilép");
    }
    public static void TowerStepMenu() throws IOException{
        System.out.println("TORONYLÉPTETÉS TESZT");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int menuChoice;
        Level level = new Level();

        try{
            do {
                System.out.println("1. Listaépítés");
                System.out.println("2. Lövés");
                System.out.println("3. Vissza a főmenübe");
                menuChoice= Integer.parseInt(br.readLine());

                switch (menuChoice) {
                    case 1:
                        buildListMenu();
                        break;
                    case 2:
                        towerShootsMenu();
                        break;
                    case 3:
                        break;
                    default:
                        System.out.println("Nincs ilyen lehetőség.");
                        break;
                }
            } while (menuChoice!=3);
        }catch(NumberFormatException nfe){
            System.err.println("Invalid Format!");
        }
    }

    public static void buildListMenu() throws IOException {
        List<MyVector> road1 = new ArrayList<MyVector>();
        List<List<MyVector>> roads  = new ArrayList<List<MyVector>>();

        for(int i = 0; i < 3; i++) {
            road1.add(new MyVector(1, i));
        }

        //a letrehozott listakat felfuzzuk a listak listajara
        roads.add(road1);

        Level level = new Level();
        level.createMap(new MyVector(3, 3), roads);
        for(int i = 0; i < 3; i++) {
            level.pushStaticEnemy(new Hobbit(), i);
        }
        Tower t = new Tower(new MyVector(0, 0));
        System.out.println("-->level.fillEnemiesInRange(t: Tower)");
        level.fillEnemiesInRange(t);
    }

    public static void towerShootsMenu() throws IOException {
        // a lövés kombinációihoz egy menü

        Tower whoShoots = null;
        Enemy whoGetsShot = null;

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int menuChoice;
        try {
            System.out.println("1. Kőtorony");
            System.out.println("2. Jégtorony");
            System.out.println("3. Nyíltorony");
            System.out.println("4. Sötér Varázslat torony");
            System.out.println("5. Energiagömb torony");
            System.out.println("6. Vissza");

            menuChoice = Integer.parseInt(br.readLine());

            switch (menuChoice) {
                case 1:
                    whoShoots = new Tower();
                    break;
                case 2:
                    whoShoots = new IceTower();
                    break;
                case 3:
                    whoShoots = new ArrowTower();
                    break;
                case 4:
                    whoShoots = new DarkTower();
                    break;
                case 5:
                    whoShoots = new EnergyTower();
                    break;
                case 6:
                    break;
                default:
                    System.out.println("Nincs ilyen lehetőség.");
                    break;
            }
        } catch (NumberFormatException nfe) {
            System.err.println("Invalid Format!");
        }

        try {
            System.out.println("1. Embert lövünk");
            System.out.println("2. Hobbitot lövünk");
            System.out.println("3. Elfet lövünk");
            System.out.println("4. Törpöt lövünk");
            System.out.println("5. Vissza");

            menuChoice = Integer.parseInt(br.readLine());

            switch (menuChoice) {
                case 1:
                    whoGetsShot = new Human();
                    break;
                case 2:
                    whoGetsShot = new Hobbit();
                    break;
                case 3:
                    whoGetsShot = new Elf();
                    break;
                case 4:
                    whoGetsShot = new Dwarf();
                    break;
                case 5:
                    break;
                default:
                    System.out.println("Nincs ilyen lehetőség.");
                    break;
            }
        } catch (NumberFormatException nfe) {
            System.err.println("Invalid Format!");
        }
        whoShoots.shoot(whoGetsShot);
    }

    public static void LevelInitMenu(){
        System.out.println("P�?LYAINIT TESZT");
        Level level = new Level();
        List<MyVector> road1 = new ArrayList<MyVector>();
        List<MyVector> road2 = new ArrayList<MyVector>();
        List<List<MyVector>> roads  = new ArrayList<List<MyVector>>();

        /*
        * Utak generalasa: ciklusokat hasznalunk az egyenes
        * utszakaszok generalasara, igy atlathatobb
        */

        //road 1 generalas
        //a dokumentacioban a sotetebbre szinezett ut
        for(int i = 0; i < 7; i++) {
            road1.add(new MyVector(2, i));
        }
        for(int i = 3; i < 6; i++) {
            road1.add(new MyVector(i, 6));
        }
        road1.add(new MyVector(5, 7));

        //road 2 generalas
        //a dokumentacioban a vilagosabbra szinezett ut
        for(int i = 0; i < 3; i++) {
            road2.add(new MyVector(2, i));
        }
        for(int i = 3; i < 6; i++) {
            road2.add(new MyVector(i, 2));
        }
        for(int i = 3; i < 8; i++) {
            road2.add(new MyVector(5, i));
        }

        //a letrehozott listakat felfuzzuk a listak listajara
        roads.add(road1);
        roads.add(road2);

        level.createMap(new MyVector(8,8), roads);
    }

    public static void BuildingMenu() throws IOException{
        System.out.println("ÉPÍTÉS TESZT");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int menuChoice;

        IBuilding whatToBuild = null;
        IBuildable whereToBuild = null;

        try{
                System.out.println("1. Torony építése");
                System.out.println("2. Akadály építése");
                System.out.println("3. Vissza a főmenübe");
                menuChoice= Integer.parseInt(br.readLine());

                switch (menuChoice) {
                    case 1:
                        whatToBuild = new Tower();
                        break;
                    case 2:
                        whatToBuild = new Obstacle();
                        break;
                    case 3:
                        break;
                    default:
                        System.out.println("Nincs ilyen lehetőség.");
                        break;
                }
        }catch(NumberFormatException nfe){
            System.err.println("Invalid Format!");
        }

        if(whatToBuild!=null) {
            try {
                System.out.println("1. Plain-re");
                System.out.println("2. Útra");
                System.out.println("3. Vissza a főmenübe");
                menuChoice = Integer.parseInt(br.readLine());

                switch (menuChoice) {
                    case 1:
                        whereToBuild = new Plain();
                        break;
                    case 2:
                        whereToBuild = new Road();
                        break;
                    case 3:
                        break;
                    default:
                        System.out.println("Nincs ilyen lehetőség.");
                        break;
                }
            } catch (NumberFormatException nfe) {
                System.err.println("Invalid Format!");
            }
        }
        // És most az építés
        if(whereToBuild!=null) whereToBuild.buildOn(whatToBuild);
    }

    public static void EnemyStepMenu() throws IOException{
        System.out.println("ELLENSÉG LÉP TESZT");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int menuChoice;

        try{
            do {
                System.out.println("1. Hobbit léptetése");
                System.out.println("2. Ember léptetése");
                System.out.println("3. Elf léptetése");
                System.out.println("4. Törp léptetése");
                System.out.println("5. Vissza a főmenübe");
                menuChoice = Integer.parseInt(br.readLine());

                List<MyVector> road1 = new ArrayList<MyVector>();
                List<MyVector> road2 = new ArrayList<MyVector>();
                List<List<MyVector>> roads  = new ArrayList<List<MyVector>>();

                //egy lehetseges ut generalasa
                for(int i = 0; i < 3; i++) {
                    road1.add(new MyVector(1, i));
                }

                //egy masik lehetoseg generalasa
                road2.add(new MyVector(1, 0));
                road2.add(new MyVector(1, 1));
                road2.add(new MyVector(2, 1));
                road2.add(new MyVector(2, 2));

                roads.add(road1);
                roads.add(road2);

                //generalunk egy egyszeru 3x3-as tesztpalyat
                Level level = new Level();
                level.createMap(new MyVector(3, 3), roads);


                switch (menuChoice) {
                    case 1: {
                        Hobbit hobbit = new Hobbit();
                        level.pushEnemy(hobbit);
                        break;
                    }

                    case 2: {
                        Human human = new Human();
                        level.pushEnemy(human);
                        break;
                    }

                    case 3: {
                        Elf elf = new Elf();
                        level.pushEnemy(elf);
                        break;
                    }

                    case 4: {
                        Dwarf dwarf = new Dwarf();
                        level.pushEnemy(dwarf);
                        break;
                    }

                    case 5: {
                        break;
                    }

                    default:
                        System.out.println("Nincs ilyen lehetőség.");
                        break;
                }
            } while (menuChoice!=5);
        }catch(NumberFormatException nfe){
            System.err.println("Invalid Format!");
        }
    }

    public static void ObstacleStepMenu() throws IOException{
        System.out.println("AKAD�?LY LÉP TESZT");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int menuChoice;
        Obstacle obstacle = new Obstacle();

        try{
            do {
                System.out.println("1. Listaépítés");
                System.out.println("2. Ütköztetés");
                System.out.println("3. Vissza a főmenübe");
                menuChoice= Integer.parseInt(br.readLine());

                switch (menuChoice) {
                    case 1:
                        break;
                    case 2:
                        obstacleCollidesMenu(obstacle);
                        break;
                    case 3:
                        break;
                    default:
                        System.out.println("Nincs ilyen lehetőség.");
                        break;
                }
            } while (menuChoice!=3);
        }catch(NumberFormatException nfe){
            System.err.println("Invalid Format!");
        }
    }

    public static void obstacleCollidesMenu(Obstacle o) throws IOException{
        Enemy whoToCollideWith = null;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("1. Emberrel");
            System.out.println("2. Hobbittal");
            System.out.println("3. Elffel");
            System.out.println("4. Törppel");
            System.out.println("5. Vissza");

            int menuChoice = Integer.parseInt(br.readLine());

            switch (menuChoice) {
                case 1:
                    whoToCollideWith = new Human();
                    break;
                case 2:
                    whoToCollideWith = new Hobbit();
                    break;
                case 3:
                    whoToCollideWith = new Elf();
                    break;
                case 4:
                    whoToCollideWith= new Dwarf();
                    break;
                case 5:
                    break;
                default:
                    System.out.println("Nincs ilyen lehetőség.");
                    break;
            }
        } catch (NumberFormatException nfe) {
            System.err.println("Invalid Format!");
        }
        o.hit(whoToCollideWith);
    }

    public static void ProjectileStepMenu() throws IOException{
        System.out.println("LÖVEDÉK LÉP TESZT");
        Projectile projectile = null;
        Enemy whoGetsShot = null;

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int menuChoice;
        try {
            System.out.println("1. Embert lövünk");
            System.out.println("2. Hobbitot lövünk");
            System.out.println("3. Elfet lövünk");
            System.out.println("4. Törpöt lövünk");
            System.out.println("5. Vissza");

            menuChoice = Integer.parseInt(br.readLine());

            switch (menuChoice) {
                case 1:
                    whoGetsShot = new Human();
                    break;
                case 2:
                    whoGetsShot = new Hobbit();
                    break;
                case 3:
                    whoGetsShot = new Elf();
                    break;
                case 4:
                    whoGetsShot = new Dwarf();
                    break;
                case 5:
                    break;
                default:
                    System.out.println("Nincs ilyen lehetőség.");
                    break;
            }
        } catch (NumberFormatException nfe) {
            System.err.println("Invalid Format!");
        }

        try {
                System.out.println("1. Kő");
                System.out.println("2. Jég");
                System.out.println("3. Nyíl");
                System.out.println("4. Sötér Varázslat");
                System.out.println("5. Energiagömb");
                System.out.println("6. Vissza");

                menuChoice = Integer.parseInt(br.readLine());


                switch (menuChoice) {
                    case 1:
                        projectile = new Projectile(whoGetsShot, 20, new MyVector(0, 2));
                        break;
                    case 2:
                        projectile = new IceCube(whoGetsShot, 20, new MyVector(0, 2));
                        break;
                    case 3:
                        projectile = new Arrow(whoGetsShot, 20, new MyVector(0, 2));
                        break;
                    case 4:
                        projectile = new DarkSpell(whoGetsShot, 20, new MyVector(0, 2));
                        break;
                    case 5:
                        projectile = new EnergyBall(whoGetsShot, 20, new MyVector(0, 2));
                        break;
                    case 6:
                        break;
                    default:
                        System.out.println("Nincs ilyen lehetőség.");
                        break;
                }
        } catch (NumberFormatException nfe2) {
            System.err.println("Invalid Format!");
        }

        List<MyVector> road1 = new ArrayList<MyVector>();
        List<List<MyVector>> roads  = new ArrayList<List<MyVector>>();

        road1.add(new MyVector(0, 0));
        road1.add(new MyVector(0, 1));
        road1.add(new MyVector(0, 2));

        roads.add(road1);

        //generalunk egy egyszeru 1x3-as tesztpalyat
        Level level = new Level();
        level.createMap(new MyVector(1,3), roads);

        level.pushEntity(projectile);
    }



    public static void main(String [] args) throws IOException, InterruptedException {

        // --lua <teszteset1.lua> --output <output1.txt>
        if(args.length == 4 && args[0].equals("--lua") && args[2].equals("--output") ) {

            System.out.println("lua");
            String test_case_filename = args[1];
            String out_file_name = args[3];

            if(out_file_name != "console") {
                FileOutputStream f = new FileOutputStream(out_file_name);
                System.setOut(new PrintStream(f));
            }
            // create an environment to run in
            Globals globals = JsePlatform.standardGlobals();

            // Use the convenience function on Globals to load a chunk.
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            globals.loadfile("BoredOfTheRings/testfw.lua").call();
            globals.loadfile("BoredOfTheRings/" + test_case_filename).call();

            while (true) {
                String line = br.readLine();

                if (line.equals("exit")) {
                    break;
                }
                try {
                    globals.load(new StringReader(line), "interpreter").call();
                }
                catch (LuaError error) {
                    System.out.println(error.getMessage());
                }
            }


            // Use any of the "call()" or "invoke()" functions directly on the chunk.
            //chunk.call( LuaValue.valueOf(script) );
        } else {

                new Game();
               /* JFrame frame = new JFrame("BoredOfTheRings");
                frame.setSize(512, 512);


                // final BufferedImage image = ImageIO.read(new File("assets\\Tower.jpg"));
                Resource.getInstance().init();

                //frame.add(pane);

                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


//            { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 3, 2 }, { 4, 2 }, { 5, 2 }, { 5, 3 }, { 5, 4 }, { 5, 5 }, { 5, 6 }, { 5, 7 } },
//            { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 2, 3 }, { 2, 4 }, { 2, 5 }, { 2, 6 }, { 3, 6 }, { 4, 6 }, { 5, 6 }, { 5, 7 } },
//        }

        Level level = new Level();
                    List<List<MyVector>> roads  = new ArrayList<List<MyVector>>();
                    List<MyVector> road1 = new ArrayList<MyVector>();
                        road1.add(new MyVector(2, 0));
                        road1.add(new MyVector(2, 1));
                        road1.add(new MyVector(2, 2));
                        road1.add(new MyVector(3, 2));
                        road1.add(new MyVector(4, 2));
                        road1.add(new MyVector(5, 2));
                        road1.add(new MyVector(5, 3));
                        road1.add(new MyVector(5, 4));
                        road1.add(new MyVector(5, 5));
                        road1.add(new MyVector(5, 6));
                        road1.add(new MyVector(5, 7));

                    roads.add(road1);

                level.createMap(new MyVector(8, 8), roads);

                GameView view = new GameView(level);
                view.addMouseListener(new GameController(level));

                frame.add(view);
                frame.pack();
                frame.setVisible(true);

                Hobbit karoly = new Hobbit();
                //karoly.init(0, new MyVector(2f, 0f));
                //karoly.setPosition(new MyVector(5f, 6f));
                karoly.setName("karoly");
                level.pushEnemy(karoly);
                level.pushEntity(karoly);
                level.setRemainingEnemies(1);*/
                /*Tower t = new Tower();
                t.setPosition(new MyVector(0, 0));
                Tile tile = level.getTile(0,0);
                tile.buildOn(t);
                t.setName("zsolti");
                level.pushEntity(t);*/
/*
                while(true)
                {
                    if(!didStep)
                    {
                        didStep = true;
                        lastStepCallTime = System.currentTimeMillis();
                    }
                    else
                    {
                        Thread.sleep(1);
                        level.step((float)(System.currentTimeMillis() - lastStepCallTime) * 0.1f);
                        lastStepCallTime = System.currentTimeMillis();
                        view.repaint();
                    }

                    //karoly.setPosition(karoly.destination);

                }*/

           // System.out.println(args[0]);
//            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//            int menuChoice;
//
//            try {
//                do {
//                    printMainMenu();
//                    menuChoice = Integer.parseInt(br.readLine());
//
//                    switch (menuChoice) {
//                        case 1:
//                            TowerStepMenu();
//                            break;
//                        case 2:
//                            LevelInitMenu();
//                            break;
//                        case 3:
//                            BuildingMenu();
//                            break;
//                        case 4:
//                            EnemyStepMenu();
//                            break;
//                        case 5:
//                            ObstacleStepMenu();
//                            break;
//                        case 6:
//                            ProjectileStepMenu();
//                            break;
//                        case 7:
//                            break;
//                        default:
//                            System.out.println("Nincs ilyen lehetőség.");
//                            break;
//                    }
//                } while (menuChoice != 7);
//            } catch (NumberFormatException nfe) {
//                System.err.println("Invalid Format!");
//            }
        }

    }
}

