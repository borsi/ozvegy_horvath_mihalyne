package BoredOfTheRings;

public interface IBuilding {

	int range = 0;
	Enemy enemiesInRange = null;

	void buildOnRoad(Tile tile);

	void buildOnPlain(Tile tile);

    String getName();
}