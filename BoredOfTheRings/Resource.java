package BoredOfTheRings;

import javax.imageio.ImageIO;
import javax.print.DocFlavor;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by borsos1zolta04 on 2014.04.27..
 */
public class Resource {
    private static final Resource INSTANCE = new Resource();

    private Resource() {}

    public static Resource getInstance() {
        return INSTANCE;
    }


    public static Image[] images = new Image[20];
    public void init() throws IOException {
        images[0] = ImageIO.read(new File("./assets/Plain.jpg"));
        images[1] = ImageIO.read(new File("./assets/Road.jpg"));
        images[2] = ImageIO.read(new File("./assets/Tower.jpg"));
        images[3] = ImageIO.read(new File("./assets/IceTower.jpg"));
        images[4] = ImageIO.read(new File("./assets/DarkTower.jpg"));
        images[5] = ImageIO.read(new File("./assets/ArrowTower.jpg"));
        images[6] = ImageIO.read(new File("./assets/EnergyTower.jpg"));
        images[7] = ImageIO.read(new File("./assets/Human.jpg"));
        images[8] = ImageIO.read(new File("./assets/Hobbit.jpg"));
        images[9] = ImageIO.read(new File("./assets/Elf.jpg"));
        images[10] = ImageIO.read(new File("./assets/Dwarf.jpg"));
        images[11] = ImageIO.read(new File("./assets/Arrow.jpg"));
        images[12] = ImageIO.read(new File("./assets/EnergyBall.jpg"));
        images[13] = ImageIO.read(new File("./assets/DarkSpell.jpg"));
        images[14] = ImageIO.read(new File("./assets/IceCube.jpg"));
        images[15] = ImageIO.read(new File("./assets/Slasher.jpg"));
        images[16] = ImageIO.read(new File("./assets/Stone.jpg"));
        images[17] = ImageIO.read(new File("./assets/Obstacle.jpg"));
        images[18] = ImageIO.read(new File("./assets/FogTower2.png"));
        images[19] = ImageIO.read(new File("./assets/MountDoom.jpg"));
    }

    public static Image getImage(int i) {
        return images[i];
    }
}
