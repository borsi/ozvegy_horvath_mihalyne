package BoredOfTheRings;

public class MyVector
{
	public float    x, y;
	
	//default constructor
	public MyVector() {
		x = y = 0;
	}
	
	//parameterized constructor
	public MyVector(float x0, float y0) {
		x = x0; y = y0;
	}
	
	//copy constructor
	MyVector(MyVector v) {
		this(v.x, v.y);
	}

    boolean equals(MyVector v) {
        if((x == v.x) && (y == v.y)) {
            return true;
        }
        else {
            return false;
        }
    }

	//add parameter vector to "this"
	void add(MyVector v) {
		x += v.x;
		y += v.y;
	}

	//add parameter float to "this"
	void add(float f) {
		x += f;
		y += f;
	}

	//substract parameter vector from "this"
	void substract(MyVector v) {
        x -= v.x;
        y -= v.y;
    }

	//substract parameter float from "this"
	void substract(float f) {
		x -= f;
		y -= f;
	}
	
	//multiply "this" by the parameter float
	void multiply(float f) {
		x *= f;
		y *= f;
	}
	
	//divide "this" by the parameter float
	void divide(float f) {
		x /= f;
		y /= f;
	}
	
	//dotProduct of "this" parameter vector
	float dotProduct(MyVector v) {
		return (x * v.x + y * v.y); 
	}

	//magnitude
	float length() {
		return (float)Math.sqrt(x * x + y * y); 
	}
	
	//normalize "this"
	void normalize() {
        if(length() != 0) {
            x /= length();
            y /= length();
        }
        else {
            x = y = 0;
        }
	}

    @Override
    public String toString() {
        return String.format("MyVector(%.1ff, %.1ff)", x, y);
    }

    public void center() {
        x = (float)(Math.floor((double)x) + 0.5);
        y = (float)(Math.floor((double)y) + 0.5);
    }
}