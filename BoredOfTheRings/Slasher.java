package BoredOfTheRings;

/**
 * Created by borsos1zolta04 on 2014.04.22..
 */
public class Slasher extends Projectile{

    public Slasher(Enemy t, float damage, MyVector pos)
    {
        dmg = damage;
        target = t;
        speed = 900;
        position = pos;
        System.out.println("\t--> <<create>> p:Slasher(" + t.getName() + "," + dmg + ", position)");
        this.img = Resource.getInstance().getImage(15);
        imgHeight = imgWidth = 28;
        setName("ZONAPARIZSISZELETELO");
    }

    @Override
    public void hit(Enemy e)
    {
        System.out.println("-->enemy.hitBySlasher(this: Slasher)");
        e.hitBySlasher(this);
        level.deleteProjectile(this);
    }

    @Override
    public MyVector getPosition()
    {
        return position;
    }

    @Override
    public String toString()
    {
        String s = String.format("<p><%s> : < \n" +
                "dmg: %.2f; \n" +
                "speed: %f; \n" +
                "position: %s; \n" +
                "target: %s; \n" +
                "name: %s \n", getClass().getSimpleName(), dmg, speed, position.toString(), target.toString(), name);

        return s;
    }
}
