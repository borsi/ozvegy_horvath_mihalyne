--az utak helyének definiálása
local roads = {
        { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 3, 2 }, { 4, 2 }, { 5, 2 }, { 5, 3 }, { 5, 4 }, { 5, 5 }, { 5, 6 }, { 5, 7 } },
        { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 2, 3 }, { 2, 4 }, { 2, 5 }, { 2, 6 }, { 3, 6 }, { 4, 6 }, { 5, 6 }, { 5, 7 } },
}

--létrehozom a pályát úttal
createMap(roads)

--akadály, ember létrehozása
o = create(Obstacle, "obstacle1")
e = createEnemy(Human, "human1")


--obstacle-t kozelebb raktam
--tile-hoz rendelem őket
add({2, 1}, o)

initEnemy({2, 0}, e, 1)
--addig kéne lépni amíg az akadályt el nem éri
for i=1, 15, 1 do
    step("ALL")
end
list("ALL")

