--az utak helyének definiálása
local roads = {
        { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 3, 2 }, { 4, 2 }, { 5, 2 }, { 5, 3 }, { 5, 4 }, { 5, 5 }, { 5, 6 }, { 5, 7 } },
        { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 2, 3 }, { 2, 4 }, { 2, 5 }, { 2, 6 }, { 3, 6 }, { 4, 6 }, { 5, 6 }, { 5, 7 } },
}

--letrehozom a palyat uttal
createMap(roads)

--torony, akadaly, hobbit létrehozása
t = create(Tower, "tower1")
o = create(Obstacle, "obstacle1")
e = createEnemy(Hobbit, "hobbit1")

--tile-hoz rendelem őket
add({1, 0}, t)
add({2, 3}, o)

--ki kell mennie a palyarol
initEnemy({5, 7}, e, 0)
--listázás
list("ALL")
-- lepes, hogy vegunk legyen
cli:getLevel():step(TIMESTEP)

