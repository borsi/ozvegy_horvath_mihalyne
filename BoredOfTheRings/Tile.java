package BoredOfTheRings;

import java.awt.*;

public abstract class Tile implements IBuildable, IUpgradeable
{
	protected Tower tower;
    protected Obstacle obstacle;
    public Image img;
	
	public void upgrade(IUpgrade u) { }
	
	public void buildOn(IBuilding b) { }
}