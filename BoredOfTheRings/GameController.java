package BoredOfTheRings;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.*;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane;
import java.awt.GridLayout;
import java.awt.Color;

/**
 * Created by borsos1zolta04 on 2014.04.27..
 */
public class GameController  implements MouseListener{

    JLabel slowRate;
    JLabel dmg_text;
    JLabel fireRate_text;
    JLabel range_text;
    Tower torony;
    Obstacle akadaly;

    final public class DmgUg implements ActionListener
    {
        public void actionPerformed(ActionEvent arg0)
        {
            if(level.player.mana >= 10) {
                torony.upgradeEffect();
                dmg_text.setText(" Damage: " + String.valueOf(torony.dmg));
                level.player.mana -= 10;
            }
        }
    }

    final public class RangeUg implements ActionListener
    {
        public void actionPerformed(ActionEvent arg0)
        {
            if(level.player.mana >= 10)
            {
                torony.upgradeRange();
                range_text.setText(" Range: " + String.valueOf(torony.range));
                level.player.mana -= 10;
            }
        }
    }

    final public class FireRateUg implements ActionListener
    {
        public void actionPerformed(ActionEvent arg0)
        {
            if(level.player.mana >= 10)
            {
                torony.upgradeRate();
                fireRate_text.setText(" Fire Rate: " + String.valueOf(torony.fireRate));
                level.player.mana -= 10;
            }
        }
    }

    final public class SlowRateUg implements ActionListener
    {
        public void actionPerformed(ActionEvent arg0)
        {
            if(level.player.mana >= 10)
            {
                akadaly.upgradeEffect();
                slowRate.setText(" Slow Rate: " + String.valueOf(akadaly.slowRate));
                level.player.mana -= 10;
            }
        }
    }

    final public class IceTowerSp implements ActionListener
    {
        public void actionPerformed(ActionEvent arg0)
        {
            if(level.player.mana >= 20)
            {
                torony.specializeIceTower();
                level.player.mana -= 20;
            }
        }
    }

    final public class ArrowTowerSp implements ActionListener
    {
        public void actionPerformed(ActionEvent arg0)
        {
            if(level.player.mana >= 20)
            {
                torony.specializeArrowTower();
                level.player.mana -= 20;
            }
        }
    }

    final public class EnergyTowerSp implements ActionListener
    {
        public void actionPerformed(ActionEvent arg0)
        {
            if(level.player.mana >= 20)
            {
                torony.specializeEnergyTower();
                level.player.mana -= 20;
            }
        }
    }

    final public class DarkTowerSp implements ActionListener
    {
        public void actionPerformed(ActionEvent arg0)
        {
            if(level.player.mana >= 20)
            {
                torony.specializeDarkTower();
                level.player.mana -= 20;
            }
        }
    }

    public GameController(Level theLevel) {
        level = theLevel;
    }

    public void TowerPanel(Tower t)
    {
        torony = t;
        Game.upgrade.removeAll();
        dmg_text = new JLabel(" Damage: " + String.valueOf(torony.dmg));
        dmg_text.setHorizontalAlignment(SwingConstants.CENTER);
        fireRate_text = new JLabel(" Fire Rate: " + String.valueOf(torony.fireRate));
        fireRate_text.setHorizontalAlignment(SwingConstants.CENTER);
        range_text = new JLabel(" Range: "+ String.valueOf(torony.range));
        range_text.setHorizontalAlignment(SwingConstants.CENTER);
        JButton dmg_gomb = new JButton("+ Damage Stone");
        JButton fireRate_gomb = new JButton("+ Fire Rate Stone");
        JButton range_gomb = new JButton("+ Range Stone");
        JButton specIceTower = new JButton("Ice Tower");
        JButton specArrowTower = new JButton("Arrow Tower");
        JButton specEnergyTower = new JButton("Energy Tower");
        JButton specDarkTower = new JButton("Dark Tower");
        JLabel spec_label = new JLabel("SPECIALIZATIONS");
        spec_label.setHorizontalAlignment(SwingConstants.CENTER);

        dmg_gomb.addActionListener(new DmgUg());
        fireRate_gomb.addActionListener(new FireRateUg());
        range_gomb.addActionListener(new RangeUg());
        specIceTower.addActionListener(new IceTowerSp());
        specArrowTower.addActionListener(new ArrowTowerSp());
        specEnergyTower.addActionListener(new EnergyTowerSp());
        specDarkTower.addActionListener(new DarkTowerSp());

        Game.upgrade.add(dmg_text);
        Game.upgrade.add(fireRate_text);
        Game.upgrade.add(range_text);
        Game.upgrade.add(dmg_gomb);
        Game.upgrade.add(fireRate_gomb);
        Game.upgrade.add(range_gomb);
        Game.upgrade.add(spec_label);
        Game.upgrade.add(specIceTower);
        Game.upgrade.add(specArrowTower);
        Game.upgrade.add(specEnergyTower);
        Game.upgrade.add(specDarkTower);
        Game.frame.pack();
    }

    public void ObstaclePanel(Obstacle o)
    {
        akadaly = o;
        Game.upgrade.removeAll();
        slowRate = new JLabel(" Slow Rate: " + String.valueOf(akadaly.slowRate));
        slowRate.setHorizontalAlignment(SwingConstants.CENTER);
        JButton slowRate_gomb = new JButton("+ Slow Rate Stone");

        slowRate_gomb.addActionListener(new SlowRateUg());

        Game.upgrade.add(slowRate);
        Game.upgrade.add(slowRate_gomb);
        Game.frame.pack();
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
        int xpos = e.getX();
        int ypos = e.getY();
        int xpos_norm = (int)((double)xpos / 75);
        int ypos_norm = (int)((double)ypos / 75);
        System.out.println(xpos + " " + ypos);
        System.out.println(xpos_norm + " " + ypos_norm);
        Tile t = level.getTile(xpos_norm, ypos_norm);

        if(MouseEvent.BUTTON1 == e.getButton())
        {
            if(t.tower != null)
            {
                TowerPanel(t.tower);
            }
            else
            {
                if(level.player.mana >= 40)
                {
                    Tower tower = new Tower(new MyVector((float)(xpos_norm), (float)(ypos_norm)));
                    t.buildOn(tower);
                }
            }

            if(t.obstacle != null)
            {
                ObstaclePanel(t.obstacle);
            }
            else
            {
                if(level.player.mana >= 40)
                {
                    Obstacle obstacle = new Obstacle(new MyVector((float) (xpos_norm), (float) (ypos_norm)));
                    t.buildOn(obstacle);
                }
            }
        }

        //tower.setPosition(new MyVector((float)(xpos_norm + 0.5), (float)(ypos_norm + 0.5)));
       // System.out.println(tower.getPosition());
        //t.buildOn(tower);
        //level.pushEntity(tower);
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    private Level level;
}
