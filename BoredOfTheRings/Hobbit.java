package BoredOfTheRings;

public class Hobbit extends Enemy {

    public Hobbit() {
        speed = defaultSpeed = 0.5f;
        health = 100;
        position = destination = new MyVector(0, 0);
        curPath = 0;
        this.img = Resource.getInstance().getImage(8);
        //randomgeneralunk egy kovetendo utat az ellensegnek
        int pathToFollow = ((int)(Math.random() * 1000.0)) % level.pathes.size();
        init(pathToFollow, level.pathes.get(pathToFollow).get(0));
    }

    public Hobbit(Hobbit e)
    {
        health = e.health;
        curPath = e.curPath;
        speed = e.speed;
        defaultSpeed = e.defaultSpeed;
        position = e.position;
        destination = e.destination;
        img = e.img;
        imgWidth = e.imgWidth;
        imgHeight = e.imgHeight;
    }

    public void hitByStone(Projectile p) {
        health -= p.dmg;
        level.player.mana += 5;
        System.out.println("-->Hobbit.checkHealth()");
        if(checkHealth())
        {
            level.deleteEnemy(this);
            level.deleteEntity(this);
        }
    }

    public void hitByDarkSpell(Projectile p) {
        health -= p.dmg * 0.75;
        level.player.mana += 5;
        System.out.println("-->Hobbit.checkHealth()");
        if(checkHealth())
        {
            level.deleteEnemy(this);
            level.deleteEntity(this);
        }
    }

    public void hitByArrow(Projectile p) {
        health -= p.dmg;
        level.player.mana += 5;
        System.out.println("-->Hobbit.checkHealth()");
        if(checkHealth())
        {
            level.deleteEnemy(this);
            level.deleteEntity(this);
        }
    }

    public void hitByEnergyBall(Projectile p) {
        health -= p.dmg;
        level.player.mana += 5;
        System.out.println("-->Hobbit.checkHealth()");
        if(checkHealth())
        {
            level.deleteEnemy(this);
            level.deleteEntity(this);
        }
    }

    public void hitByIceCube(Projectile p) {
        health -= p.dmg * 1.25;
        level.player.mana += 5;
        System.out.println("-->Hobbit.checkHealth()");
        if(checkHealth())
        {
            level.deleteEnemy(this);
            level.deleteEntity(this);
        }
    }

    public void hitBySlasher(Projectile p)
    {
        System.out.println("-->Human sliced in half! Pos: (" + position.x + ", " + position.y + "), Dest: (" + destination.x + ", " + destination.y + ")" );
        health /=2;
        Hobbit cl = new Hobbit(this);
        cl.setName(this.getName() + "_SLASHED");
        level.player.mana += 5;
        level.pushEnemy(cl);
        level.setRemainingEnemies(level.getRemainingEnemies() + 1);
        checkHealth();
    }

    public String toString() {
    	String s = String.format("<e><%s> : < \n" +
                "defaultSpeed : %f; \n" +
                "speed: %f; \n" +
                "health: %d; \n" +
                "curPath: %d; \n" +
                "position: %s; \n" +
                "destination: %s; \n" +
                "name: %s \n", getClass().getSimpleName(), defaultSpeed, speed, health, curPath, position.toString(), destination.toString(), name);
        return s;
    }
}