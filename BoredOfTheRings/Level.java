package BoredOfTheRings;

import com.sun.org.apache.xml.internal.serializer.utils.SystemIDResolver;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import  java.util.List;
import java.util.Random;
import javax.swing.JOptionPane;

public class Level
{

	private List<Enemy> enemies;
    private List<Enemy> enemiesInBuffer;

	private Tile[][] tiles;
	public List<List<MyVector>> pathes;

    private int remainingEnemies = 0, maxEnemies = 0;

    private List<Entity> entities;
    private List<Entity> newEntities;
    private List<Entity> removedEntities;

    protected static Player player = new Player();
    private GameView view = new GameView(this);

    private long lastWaveCreationTime = 0;
    private long waveCreationDelay = 6000;
    private long lastEnemyCreationTime = 0;
    private long enemyCreationDelay = 600;

    public Level()
    {
        System.out.println("\t\t<-- Level object created");
        enemies = new ArrayList<Enemy>();
        enemiesInBuffer = new ArrayList<Enemy>();

        pathes = new ArrayList<List<MyVector>>();

        entities = new ArrayList<Entity>();
        newEntities = new ArrayList<Entity>();
        removedEntities = new ArrayList<Entity>();

        Entity.level = this;
        maxEnemies = remainingEnemies = 12;
    }

	public void createMap(MyVector size, List<List<MyVector>> roads)
    {
        pathes = roads;
        tiles = new Tile[(int)size.x][(int)size.y];
		for(int i = 0; i < size.x; i++) {
            for(int j = 0; j < size.y; j++)
            {
                //ez a valtozo fogja eldonteni, hogy utunk vagy siksagunk lesz-e
                boolean contains = false;
                boolean lastRoadTile = false;
                //tovabbi hasznalatra eltaroljuk a jelenlegi indexpart
                MyVector currentPosition = new MyVector((float)i, (float)j);
                //vegigmegyunk minden uton
                for(Iterator<List<MyVector>> iter = roads.iterator(); iter.hasNext();)
                {
                    List<MyVector> currentRoadList = iter.next();
                    //vegigmegyunk az aktualis ut minden utelemen
                    for(Iterator<MyVector> iter2 = currentRoadList.iterator(); iter2.hasNext();)
                    {
                        MyVector currentRoadVector = iter2.next();
                        //ha az aktualis utelem azonos a jelenlegi indexparral, utat kell krealni
                        if(currentRoadVector.x == currentPosition.x && currentRoadVector.y == currentPosition.y)
                        {
                            if(!iter2.hasNext())
                            {
                                lastRoadTile = true;
                            }
                            contains = true;
                            break;
                        }
                    }
                    //ha kiderult a Tile-rol, hogy ut, akkor tovabbi vizsgalat nem szukseges
                    if(contains)
                    {
                        break;
                    }
                }
                //ha ut, akkor letrehozzuk
                if(contains)
                {
                    System.out.println("--> Creating Road at: " + i + ", " + j);
                    Road road = new Road();
                    //beletesszuk az utak listajaba is
                    tiles[i][j] = road;
                    if(lastRoadTile)
                    {
                        road.img = Resource.getInstance().getImage(19);
                    }
                }
                //egyebkent siksagot hozunk letre
                else
                {
                    System.out.println("--> Creating Plain at: " + i + ", " + j);
                    tiles[i][j] = new Plain();
                }
            }
        }
	}

    public int getRemainingEnemies() {
        return remainingEnemies;
    }

    public void setRemainingEnemies(int remainingEnemies) {
        this.remainingEnemies = remainingEnemies;
    }

    public List<Entity> getEntities()
    {
        addAndRemoveEntities();
        return entities;
    }

    public Tile getTile(int x, int y) {
        return tiles[x][y];
    }

	public synchronized void deleteEntity(Entity e)
    {

        for (Iterator<Entity> iter = entities.iterator(); iter.hasNext(); )
        {
            if (iter.next().equals(e))
            {
                System.out.println("REMOVED ENTITY");
                removedEntities.add(e);
                return;
            }
        }
	}

    public synchronized void deleteEnemy(Enemy e)
    {
        for(Iterator<Enemy> iter = enemies.iterator(); iter.hasNext();)
        {
            if(iter.next().equals(e))
            {
                System.out.println("REMOVED ENEMY");

                enemies.remove(e);
                this.setRemainingEnemies(this.getRemainingEnemies()-1);
                return;
            }
        }
    }

	public synchronized void pushEntity(Entity e)
    {
        System.out.println("PUSHED ENTITY");
		newEntities.add(e);
	}

	public synchronized void pushEnemy(Enemy e)
    {
        System.out.println("PUSHED ENEMY");
        enemies.add(e);
        newEntities.add(e);
	}

    public synchronized void pushStaticEnemy(Enemy e, int index)
    {
        /*//randomgeneralunk egy kovetendo utat az ellensegnek
        Random generator = new Random(12312333);
        int pathToFollow = generator.nextInt(10000) % pathes.size();

        e.init(pathToFollow, pathes.get(pathToFollow).get(index));
        System.out.println("PUSHED ENEMY");
        enemies.add(e);
        newEntities.add(e);*/
    }

	public MyVector getNextDestination(int curPath, MyVector position)
    {
        List<MyVector> currentRoadList = pathes.get(curPath);
        //vegigmegyunk az aktualis ut minden utelemen
        for(Iterator<MyVector> iter = currentRoadList.iterator(); iter.hasNext();)
        {
            MyVector currentRoadVector = iter.next();
            //ha az aktualis utelem azonos a jelenlegi indexparral, visszaterunk a kovetkezo utelemmel
            if(currentRoadVector.x == position.x && currentRoadVector.y == position.y)
            {
                if(iter.hasNext())
                {
                    MyVector nextDest = iter.next();
                    System.out.println("\t<-- (" + (int)nextDest.x + ", " + (int)nextDest.y + ") is the next road index");
                    return nextDest;
                }
            }
        }
        //ha nem talaltuk meg a kovetkezo utat
        // elertunk a vegzet hegyehez
        System.out.println("\t<-- (-1, -1)  No route found, we must be at mount doom");
        player.setDead();
        return new MyVector(-1, -1);
	}

	public void fillEnemiesInRange(Tower t)
    {
        t.enemiesInRange.clear();

        for(Iterator<Enemy> iter = enemies.iterator(); iter.hasNext();)
        {
            Enemy e = iter.next();
            //System.out.println("-->" + t.getName() + ": is " + e.getName() + " in range?");
            MyVector temp = new MyVector(t.position);
            temp.substract(e.position);

            if((temp.length() < t.range))
            {
                if(t.enemiesInRange != null)
                {
                    if(!t.enemiesInRange.contains(e))
                    {
                        //System.out.println("\t\t<--Enemy in range!");
                        t.enemiesInRange.add(e);
                    }
                }
                else
                {
                    //System.out.println("\t\t<--Enemy in range!");
                    t.enemiesInRange.add(e);
                }
            }
        }
	}

    public void fillEnemiesInRange(Obstacle t)
    {
        t.enemiesInRange.clear();

        for(Iterator<Enemy> iter = enemies.iterator(); iter.hasNext();)
        {
            Enemy e = iter.next();
           // System.out.println("-->" + t.getName() + ": is " + e.getName() + " in range?");
            MyVector temp = new MyVector(t.position);
            temp.substract(e.position);

            if((temp.length() < t.range))
            {
                if(t.enemiesInRange != null)
                {
                    if(!t.enemiesInRange.contains(e))
                    {
                        //System.out.println("\t\t<--Enemy in range!");
                        t.enemiesInRange.add(e);
                    }
                }
                else {
                   // System.out.println("\t\t<--Enemy in range!");
                    t.enemiesInRange.add(e);
                }
            }
        }
    }

    public void addAndRemoveEntities()
    {
        synchronized(entities)
        {
            synchronized(newEntities)
            {
                for (Iterator<Entity> iter = newEntities.iterator(); iter.hasNext(); )
                {
                    Entity e = iter.next();
                    entities.add(e);
                }
                newEntities.clear();
            }
        }

        synchronized(entities)
        {
            synchronized (removedEntities)
            {
                for (Iterator<Entity> iter = removedEntities.iterator(); iter.hasNext(); )
                {
                    Entity e = iter.next();
                    entities.remove(e);
                }
                removedEntities.clear();
            }
        }
    }

    public void step(float dt)
    {
        // ellenoriznunk kell a vege felteteleket
        view.repaint();
        if((getRemainingEnemies() > 0) && (!player.getPlayerState()))
        {
            // ha nincs vege, leptetunk:
            // a level stepjeben vegigiteralunk az osszes entitason
            // es leptetunk
            synchronized(enemiesInBuffer)
            {
                if(System.currentTimeMillis() > lastWaveCreationTime + waveCreationDelay && maxEnemies > 0)
                {
                    int enemyCount;
                    if(lastWaveCreationTime == 0)
                    {
                        enemyCount = 1;
                    }
                    else
                    {
                        enemyCount = (((int)(Math.random() * 1000.0)) % 3) + 1;
                    }
                    createWave(enemyCount);
                    lastWaveCreationTime = System.currentTimeMillis();
                }
            }

            synchronized(enemiesInBuffer)
            {
                if (System.currentTimeMillis() > lastEnemyCreationTime + enemyCreationDelay && !enemiesInBuffer.isEmpty())
                {
                    pushEnemy(enemiesInBuffer.get(0));
                    enemiesInBuffer.remove(0);
                    lastEnemyCreationTime = System.currentTimeMillis();
                }
            }

            addAndRemoveEntities();
            synchronized(entities)
            {
                for(Iterator<Entity> iter = entities.iterator(); iter.hasNext();)
                {
                    Entity e = iter.next();
                    e.step(dt);
                }
            }
        }
        else if(getRemainingEnemies() <= 0)
        {
            wonGame();
        }
        else if(player.getPlayerState())
        {
            lostGame();
        }
        view.repaint();
    }

    public void lostGame()
    {
        System.out.println("\t\t END OF GAME, PLAYER LOST!");
        JOptionPane.showMessageDialog(null, "END OF GAME, PLAYER LOST!", "You Lose!", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }

    public void wonGame()
    {
        System.out.println("\t\t END OF GAME, PLAYER WON!");
        JOptionPane.showMessageDialog(null, "END OF GAME, PLAYER WON!", "You Win!", JOptionPane.INFORMATION_MESSAGE);
        System.exit(1);
    }

    public void saveToFile(String fileName) throws FileNotFoundException, UnsupportedEncodingException {
        JSONSerializer serializer = new JSONSerializer();
        PrintWriter p = new PrintWriter(fileName, "UTF-8");
        p.println(serializer.prettyPrint(true).deepSerialize(this));
        p.flush();
    }

    public static Level loadFromFile(String fileName) throws FileNotFoundException {
        JSONDeserializer<Level> deserializer = new JSONDeserializer<Level>();
        FileReader f = new FileReader(fileName);
        return deserializer.deserialize(f);
    }

	public synchronized void deleteProjectile(Projectile p) {
       //a mostani celnak megfelelo gyorsabb megoldas
       // entities.clear();

        for(Iterator<Entity> iter = entities.iterator(); iter.hasNext();)
        {
            if(iter.next().equals(p))
            {
                removedEntities.add(p);
                break;
            }
        }
	}

    @Override
    public String toString()
    {
        String s = String.format("Remaining enemies: %d", this.getRemainingEnemies());
        return s;
    }

    public Tile[][] getTiles()
    {
        return tiles;
    }

    public synchronized void createWave(int enemyCount)
    {
        System.out.println("CREATING WAVE");

        int enemyToGenerate;
        for(int i = 0; i < enemyCount; i++)
        {
            enemyToGenerate = ((int)(Math.random() * 1000.0)) % 4;
            maxEnemies --;
            if(maxEnemies < 0)
            {
                return;
            }
            switch(enemyToGenerate)
            {
                case 0:
                {
                    Elf elf = new Elf();
                    elf.setName("elf");
                    enemiesInBuffer.add(elf);
                    break;
                }
                case 1:
                {
                    Human human = new Human();
                    human.setName("human");
                    enemiesInBuffer.add(human);
                    break;
                }
                case 2:
                {
                    Hobbit hobbit = new Hobbit();
                    hobbit.setName("hobbit");
                    enemiesInBuffer.add(hobbit);
                    break;
                }
                case 3:
                {
                    Dwarf dwarf = new Dwarf();
                    dwarf.setName("dwarf");
                    enemiesInBuffer.add(dwarf);
                    break;
                }
            }
        }
    }
}