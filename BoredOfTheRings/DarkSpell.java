package BoredOfTheRings;

public class DarkSpell extends Projectile {

	public DarkSpell(Enemy t, float damage, MyVector pos) {
        dmg = damage;
        target = t;
        speed = 900;
        position = pos;
        System.out.println("\t--> <<create>> p:DarkSpell("+ t.getName() + "," + dmg + ", position)");
        this.img = Resource.getInstance().getImage(13);
        imgHeight = imgWidth = 28;
	}

    @Override
    public void hit(Enemy e) {
        System.out.println("-->enemy.hitByDarkSpell(this: DarkSpell)");
        e.hitByDarkSpell(this);
        level.deleteProjectile(this);
    }

	/*public void step(float dt) {
        MyVector offset = new MyVector(target.position);
        offset.substract(position);
        offset.normalize();
        offset.multiply(dt/1000f * speed);
        position.add(offset);

        MyVector newOffset = new MyVector(target.position);
        newOffset.substract(position);
        newOffset.normalize();

        //Ha elojelet valtottunk, kerjuk a kovetkezo celpontot.
        if((offset.x > 0 && newOffset.x <= 0) || (offset.x <= 0 && newOffset.x > 0) || (offset.y > 0 && newOffset.y <= 0) || (offset.y <= 0 && newOffset.y > 0)) {
            target.hitByDarkSpell(this);
            System.out.println("-->enemy.hitByDarkSpell(this: DarkSpell)");
        }
	}*/

    @Override
    public String toString() {
        String s = String.format("<p><%s> : < \n" +
                "dmg: %d; \n" +
                "speed: %f; \n" +
                "position: %s; \n" +
                "target: %s; \n" +
                "name: %s \n", getClass().getSimpleName(), dmg, speed, position.toString(), target.toString(), name);

        return s;
    }
}