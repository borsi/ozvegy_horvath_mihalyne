package BoredOfTheRings;

import BoredOfTheRings.ITargeting.*;

import java.util.ArrayList;
import java.util.List;

public class Obstacle extends Entity implements IBuilding, IUpgrade, ITargeting {

	float slowRate;
    float range = 0;
	protected List<Enemy> enemiesInRange = null;

    public Obstacle()
    {
        range = 1;
        slowRate = 0.2f;
        enemiesInRange =  new ArrayList<Enemy>();
        img = Resource.getInstance().getImage(17);
        imgWidth = 75;
        imgHeight = 75;
    }

    public Obstacle(MyVector v)
    {
        position = v;
        range = 1;
        slowRate = 0.2f;
        enemiesInRange =  new ArrayList<Enemy>();
        img = Resource.getInstance().getImage(17);
        imgWidth = 75;
        imgHeight = 75;
    }

    public void upgradeRange(){
        range *= 1.1;
    }

    public void upgradeEffect()
    {
        slowRate *= 1.1;
        System.out.println("\t <-- Lassítás mértéke fejlesztve!" + slowRate);
    }

    public void upgradeRate(){
        slowRate *= 1.1;
    }

    @Override
    public void setPosition(MyVector v)
    {
        v.center();
        super.setPosition(v);
    }

    public String getEnemiesInRangeStr() {
        StringBuilder sb = new StringBuilder();
        if(enemiesInRange.size() != 0) {
            sb.append("[");
            for (Entity e : enemiesInRange) {
                sb.append(String.format("\"%s\"", e.getName()));
                sb.append(", ");
            }
            String s = sb.toString().substring(0, sb.length() - 2) + "]";
            return s;
        }
        else  return "[]";
    }

    @Override
	public void hit(Enemy e) { e.hitByObstacle(slowRate); }

	@Override
	public void collide(Enemy e) { }

    @Override
    public void buildOnRoad(Tile tile)
    {
        tile.obstacle = this;
        level.player.mana -= 40;
        level.pushEntity(this);
    }

    @Override
    public void buildOnPlain(Tile tile)
    {

    }

    @Override
    public void step(float dt) {
        // a torony lepese metodus
        // megtolti a torony enemy tarolojat a
        // level ellensegekkel

        level.fillEnemiesInRange(this);
        // a firerate szamlalot csokkentjuk

        // ha lejar a szamlalo, lovunk, ha van valaki a range-ben
        // ha nincs senki a range-ben, a timer marad 0, vagy az alatt
        // a torony "csore van toltve"
        if(!enemiesInRange.isEmpty()) {
            enemiesInRange.get(0).hitByObstacle(slowRate);
            System.out.println(this.getName() + " is slowing " + enemiesInRange.get(0).getName() + " by " + slowRate);
            System.out.println(enemiesInRange.get(0).getName() + " has a speed of " + enemiesInRange.get(0).speed);
        }
    }

    @Override
    public String toString(){
        String s = String.format("<o><%s> : < \n" +
                "enemiesInRange : %s; \n" +
                "slowRate: %.2f; \n" +
                "range: %f; \n" +
                "position: %s; \n" +
                "name: %s \n", getClass().getSimpleName(), getEnemiesInRangeStr(), slowRate, range, position.toString(), name);

        return s;
    }
}