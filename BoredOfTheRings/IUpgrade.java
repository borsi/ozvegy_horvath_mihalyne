package BoredOfTheRings;

public interface IUpgrade {

	public void upgradeRange();

	public void upgradeEffect();

	public void upgradeRate();

}