package BoredOfTheRings;

import java.util.ArrayList;
import java.util.List;

public class DarkTower extends Tower {

    public DarkTower() {
        enemiesInRange = new ArrayList<Enemy>();
        System.out.println("--> <<create>> t:DarkTower");
        dmg = 30;
        fireRate = 210;
        range = 2;
        position = new MyVector(0, 0);
        fogTime = 0.0f;
        fog = 1.0f;
        temp_range = range;
        img = Resource.getInstance().getImage(4);
        imgWidth = 64;
        imgHeight = 64;
    }

    public DarkTower(MyVector p) {
        enemiesInRange = new ArrayList<Enemy>();
        System.out.println("--> <<create>> t:DarkTower");
        dmg = 30;
        fireRate = 210;
        range = 2;
        position = p;
        fogTime = 0.0f;
        fog = 1.0f;
        temp_range = range;
        img = Resource.getInstance().getImage(4);
        imgWidth = 64;
        imgHeight = 64;
    }

    public Projectile shoot(Enemy enemy)
    {
        System.out.print("t:IceTower");
        // random generalunk slashert vagy nem.
        Projectile p;
        int randomNum = (int)(Math.random()*100);
        MyVector m = new MyVector(this.position);
        if(randomNum > 95)
        {
            System.out.println("SLAAAASHEEEEEEEEEEEER");
            p = new Slasher(enemy, this.dmg, m);
        }
        else
        {
            p = new DarkSpell(enemy, this.dmg, m);
        }
        System.out.println("<-- icecube created");
        return p;
    }
	
	@Override
	public void buildOnRoad(Tile tile) {
		
		
	}

	@Override
	public void buildOnPlain(Tile tile)
    {
        level.pushEntity(this);
	}

    @Override
    public void setPosition(MyVector v)
    {
        v.center();
        super.setPosition(v);
    }

	@Override
    public String toString(){
        String s = String.format("<t><%s> : < \n" +
                "enemiesInRange : %s; \n" +
                "dmg: %f; \n" +
                "fireRate: %f; \n" +
                "range: %f; \n" +
                "position: %s; \n" +
                "fogTime: %f; \n" +
                "fog: %f; \n" +
                "name: %s \n", getClass().getSimpleName(), getEnemiesInRangeStr(), dmg, fireRate, range, position.toString(), fogTime, fog,  name);

        return s;
    }
}