package BoredOfTheRings;

public interface IBuildable {

	void buildOn(IBuilding b);

}