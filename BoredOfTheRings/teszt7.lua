--az utak helyének definiálása
local roads = {
        { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 3, 2 }, { 4, 2 }, { 5, 2 }, { 5, 3 }, { 5, 4 }, { 5, 5 }, { 5, 6 }, { 5, 7 } },
        { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 2, 3 }, { 2, 4 }, { 2, 5 }, { 2, 6 }, { 3, 6 }, { 4, 6 }, { 5, 6 }, { 5, 7 } },
}

--létrehozom a pályát úttal
createMap(roads)

--torony, akadály, törp létrehozása
t = create(Tower, "tower1")
o = create(Obstacle, "obstacle1")
e = createEnemy(Dwarf, "dwarf1")

--tile-hoz rendelem őket
add({1, 0}, t)
add({2, 3}, o)
--setPosition({2, 0}, e)

initEnemy({2,0}, e, 1)
--slasher lövedeket kéne csinálni és azzal eltaláltatni

--beallitjuk, hogy slasher-t generaljunk
cli:getLevel():setTestSlasher(true)
for i=1, 8, 1 do
    print("\n" .. i .. ". step")
    step("ALL")
end
list("ALL")

--listázás
--list("tower1")
--list("obstacle1")
--list("human1")
--list("level")
