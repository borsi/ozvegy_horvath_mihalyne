--az utak helyének definiálása
local roads = {
        { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 3, 2 }, { 4, 2 }, { 5, 2 }, { 5, 3 }, { 5, 4 }, { 5, 5 }, { 5, 6 }, { 5, 7 } },
        { { 2, 0 }, { 2, 1 }, { 2, 2 }, { 2, 3 }, { 2, 4 }, { 2, 5 }, { 2, 6 }, { 3, 6 }, { 4, 6 }, { 5, 6 }, { 5, 7 } },
}

--létrehozom a pályát úttal
createMap(roads)

--torony, akadály, ember létrehozása
t = create(ArrowTower, "tower1")
o = create(Obstacle, "obstacle1")
e = createEnemy(Human, "human1")
p1 = t:shoot(e)
p2 = t:shoot(e)
p3 = t:shoot(e)



--tile-hoz rendelem őket
add({1, 0}, t)
add({2, 3}, o)
setPosition({2, 0}, e)
-- egy ellensegunk van, akit leszedunk
cli:getLevel():setRemainingEnemies(1)

list("human1")
e:hitByArrow(p1)
list("human1")
e:hitByArrow(p2)
e:hitByArrow(p3)

level = cli:getLevel()
--meg kell halnia az enemynek
list("level")
level:step(TIMESTEP)

--listázás

